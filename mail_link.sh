#!/bin/bash

cd /opt/tomcat/webapps

sudo cp mails.json /home/site/covidMX/mail

cd /home/site/covidMX/mail

chown site:site mails.json

ansible-playbook mail_link.yml
